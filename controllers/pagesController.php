<?php

Namespace Sebastian\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


use Philo\Blade\Blade;


class PagesController
{
	protected $views = '../views';
	protected $cache = '../cache';
	protected $blade;

	public function __construct()
	{
		$this->blade = new Blade($this->views, $this->cache);
	}

	/*public function about(ServerRequestInterface $request, ResponseInterface $response)
	{
		return $pagina = $this->blade->view()->make('about')->render();
		$response->getBody()->write($pagina);
		return $response;
	}*/

	public function about()
	{
		return $this->blade->view()->make('about')->render();
	}

	public function jquery()
	{
		return $this->blade->view()->make('jquery')->render();
	}

	public function user(ServerRequestInterface $request, ResponseInterface $response, array $args)
	{
		return '<h1>Hola '.$args['name'];
	}	
}