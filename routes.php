<?php
$route->map('GET', '/pages/about', 'Sebastian\Controllers\pagesController::about');
$route->map('GET', '/user/{name}', 'Sebastian\Controllers\pagesController::user');
$route->map('GET', '/pages/jquery', 'Sebastian\Controllers\pagesController::jquery');