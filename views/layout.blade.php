<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">  
    <link rel="stylesheet" href="/vendor/bootstrap/dist/css/bootstrap.min.css">
    <script type="text/javascript" src="/vendor/jquery/dist/jquery.min.js"></script>
    <title>Document</title>
</head>

<body>
    
    <div class="container">
        @yield('content')
    </div>
    
    @yield('footer')
    
</body>
    
</html>